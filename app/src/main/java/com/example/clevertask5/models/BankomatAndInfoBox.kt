package com.example.clevertask5.models
import com.google.gson.annotations.SerializedName

data class BankomatAndInfoBox(
    @SerializedName("gps_x") val gps_x: Double,
    @SerializedName("gps_y") val gps_y: Double
)
