package com.example.clevertask5.models

import com.google.gson.annotations.SerializedName

class Filial(@SerializedName("GPS_X") val gps_x: Double,
             @SerializedName("GPS_Y") val gps_y: Double)