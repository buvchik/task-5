package com.example.clevertask5

import com.example.clevertask5.models.BankomatAndInfoBox
import com.example.clevertask5.models.Filial
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET

interface BankService {
@GET("atm?city=Гомель")
 fun  getAtmList(): Observable<List<BankomatAndInfoBox>>
 @GET("infobox?city=Гомель")
 fun  getInfoBoxList(): Observable<List<BankomatAndInfoBox>>
 @GET("filials_info?city=Гомель")
 fun  getFilialsInfoList(): Observable<List<Filial>>
}