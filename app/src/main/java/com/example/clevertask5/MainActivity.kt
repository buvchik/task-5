package com.example.clevertask5

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.clevertask5.databinding.ActivityMainBinding
import com.example.clevertask5.models.BankomatAndInfoBox
import com.example.clevertask5.models.Filial
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import kotlin.math.pow
import kotlin.math.sqrt

class MainActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        val url = "https://belarusbank.by/api/"

        mMap = googleMap
        val point = LatLng(52.425163, 31.015039)
        val icon = BitmapDescriptorFactory.fromResource(R.drawable.point)

        mMap.addMarker(MarkerOptions().position(point).title(getString(R.string.city))).setIcon(icon)
        mMap.moveCamera(CameraUpdateFactory.newLatLng(point))
        mMap.setMinZoomPreference(10f)

        val retrofit = Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).addCallAdapterFactory(RxJava3CallAdapterFactory.create()).build()
        val bankService = retrofit.create(BankService::class.java)

        val atmObservable = bankService.getAtmList()
        val infoBoxObservable = bankService.getInfoBoxList()
        val filialObservable = bankService.getFilialsInfoList()

       Observable.concat(atmObservable, infoBoxObservable, filialObservable)
                .subscribe(object : Observer<List<Any>> {
                    var listAllItem = mutableListOf<BankomatAndInfoBox>()

                    override fun onSubscribe(d: Disposable) {
                    }

                    override fun onError(e: Throwable) {
                    }

                    override fun onComplete() {

                        Observable.fromArray(listAllItem)
                                .subscribeOn(AndroidSchedulers.mainThread())
                                .flatMapIterable { list -> list }
                                .toSortedList { p0, p1 ->
                                    val dist1 = sqrt((point.latitude - p0.gps_x).pow(2.0) + (point.longitude - p0.gps_y).pow(2.0))
                                    val dist2 = sqrt((point.latitude - p1.gps_x).pow(2.0) + (point.longitude - p1.gps_y).pow(2.0))
                                    dist1.compareTo(dist2)
                                }.toObservable()
                                .flatMapIterable { list -> list }
                                .take(11) // почему то -1
                                .subscribe { s ->
                                    mMap.addMarker(
                                        MarkerOptions().position(
                                            LatLng(
                                                s.gps_x,
                                                s.gps_y
                                            )
                                        )
                                    )
                                }
                    }

                    override fun onNext(t: List<Any>) {
                        t.forEach {
                            when (it) {
                                is BankomatAndInfoBox -> listAllItem.add(it)
                                is Filial -> listAllItem.add(BankomatAndInfoBox(it.gps_x, it.gps_y))
                            }
                        }
                    }
                })


    }

}